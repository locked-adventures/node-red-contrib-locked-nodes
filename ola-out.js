module.exports = function(RED) {
    class OlaOutNode {
        constructor(config) {
            RED.nodes.createNode(this, config);

            /** @type{OladNode} */ this.universe = RED.nodes.getNode(config.universe);

            this.values = JSON.parse(config.values);

            this.on('input', function(msg) {
                let payload = msg.payload;
                let isInject = false;
                if (typeof(payload) == "object") {
                    this.universe.sendDmx(payload);
                }
                else if (payload == 'start') {
                    this.universe.sendDmx(this.values);
                }
                else if (payload == 'inject') {
                    this.universe.sendDmx(this.values);
                    isInject = true;
                }
                if (!isInject) {
                    this.send(msg);
                }
            });
        }
    }

    RED.nodes.registerType("ola out", OlaOutNode);

    RED.httpAdmin.post("/ola_out/:id", RED.auth.needsPermission("ola_out.write"), function(req, res) {
        var node = RED.nodes.getNode(req.params.id);
        if (node != null) {
            try {
                if (req.body) {
                    node.receive(req.body);
                } else {
                    node.receive();
                }
                res.sendStatus(200);
            } catch(err) {
                res.sendStatus(500);
                node.error(RED._("inject.failed",{error: err.toString()}));
            }
        }
        else {
            res.sendStatus(404);
        }
    });
};
