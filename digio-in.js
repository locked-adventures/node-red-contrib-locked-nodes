module.exports = function(RED) {
    class DigioInNode {
        constructor(n) {
            RED.nodes.createNode(this, n);
            this._name = n.name;

            /** @type{DigioConnectionNode} */ this._digio = RED.nodes.getNode(n.digio);
            this._values = n.values;

            this._digio.on('digio-input', this._onDigioInput.bind(this));
            this._digio.on('digio-status', s => this.status(s))
            this.status(this._digio.digioStatus);
        }

        _onDigioInput(inputObject) {
            this.send({payload: inputObject});
        }
    }

    RED.nodes.registerType("digio in", DigioInNode);
};
