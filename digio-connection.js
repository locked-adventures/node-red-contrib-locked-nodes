module.exports = function(RED) {
    const { SerialPort } = require('serialport');
    const { ReadlineParser } = require('@serialport/parser-readline')
    // const { EventEmitter } = require('node:events');

    class DigioConnectionNode {
        constructor(n) {
            RED.nodes.createNode(this, n);
            this._name = n.name;

            this._synchronized = false;
            this._retryTimeoutId = null;
            this._retryTimeout = n.retryInterval;
            this._exit = false;

            this._serialport = new SerialPort({ path: String(n.path), baudRate: Number(n.baud) });
            const parser = this._serialport.pipe(new ReadlineParser({ delimiter: '\r\n' }));

            this._serialport.on('open', this._onSerialOpen.bind(this));
            this._serialport.on('close', this._onSerialClose.bind(this));
            this._serialport.on('error', this._onSerialError.bind(this));
            parser.on('data', this._onSerialReadline.bind(this));

            this._setDigioStatus({fill: "yellow", shape: "dot", text: "connecting..."});
            this.on('close', this._onNodeClose.bind(this));
        }

        get digioStatus() {
            return this._digioStatus;
        }

        _setDigioStatus(status) {
            this._digioStatus = status;
            this.emit('digio-status', this._digioStatus);
        }

        _onNodeClose() {
            this._exit = true;
            clearTimeout(this._retryTimeoutId);
            this._serialport.close();
        }

        _onSerialOpen() {
            clearTimeout(this._retryTimeoutId);
            this.debug("Serial port opened");
            this._setDigioStatus({fill: "blue", shape: "dot", text: "synchronizing..."});
        }

        _onSerialClose() {
            this._synchronized = false;
            this.warn("Serial port closed");
            this._setDigioStatus({fill: "red", shape: "dot", text: "disconnected"});
            if (this._exit) return;
            this._retryTimeoutId = setTimeout(
                this._onSerialRetry.bind(this),
                this._retryTimeout
            )
        }

        _onSerialError(error) {
            this.error(`Serial port error: ${error}`);
            this._setDigioStatus({fill: "red", shape: "dot", text: "error"});
            if (this._exit) return;
            this._retryTimeoutId = setTimeout(
                this._onSerialRetry.bind(this),
                this._retryTimeout
            )
        }

        _onSerialRetry() {
            this.debug("Retrying to open port");
            this._serialport.open();
        }

        _onSerialReadline(line) {
            this.trace(`Serial line received: ${line}`);
            if (line == "BEGIN") {
                this._synchronized = true;
                this.debug("Synchronized with DIGIO device");
                this._setDigioStatus({fill: "green", shape: "dot", text: "connected"});
            }
            else if (this._synchronized) {
                try {
                    const digioInput = JSON.parse(line);
                    this.emit('digio-input', digioInput);
                }
                catch(e) {
                    if (e instanceof SyntaxError) {
                        this.error(`Cannot parse DIGIO input: ${e}`)
                    }
                    else {
                        this.error(`onSerialReadline error: ${e}`);
                    }
                }
            }
        }

        setOutput(outputValues) {
            const payload = JSON.stringify({out: outputValues}) + "\r\n";
            this.trace(`Writing to serial port: ${payload}`);
            this._serialport.write(payload);
        }

        write(data) {
            this._serialport.write(data);
        }

        get name() {
            return this._name;
        }

        get path() {
            return this._serialport.path;
        }

        get baudRate() {
            return this._serialport.baudRate;
        }
    }

    RED.nodes.registerType("digio-connection", DigioConnectionNode);
}
