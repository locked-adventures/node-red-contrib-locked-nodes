const { spawn } = require('node:child_process');

module.exports = function(RED) {
    class PlayMultichannelAudioNode {
        constructor(config) {
            RED.nodes.createNode(this, config);
            this.defaultFile = config.file;
            this.defaultRemix = JSON.parse(config.remix);
            this.children = {};

            this.on('input', function(msg) {
                switch(msg.payload) {
                    case "inject": // toggle activity
                        if (!Object.keys(this.children).length) {
                            this.spawnSox();
                        }
                        else {
                            this.killOldChildren();
                        }
                        break;
                    case "start":
                        this.killOldChildren();
                        this.spawnSox(msg);
                        break;
                    case "stop":
                        this.killOldChildren();
                    default:
                        this.send(msg);
                }
            });
            this.on('close', () => {
                this.killOldChildren();
            });
        }

        killOldChildren() {
            for (let pid in this.children) {
                let signal = 'SIGKILL';
                if (!this.children[pid].killed) { // Kill child if it was not already killed
                    if (this.children[pid].process.kill(signal)) {
                        this.debug(`Sent ${signal} to sox child with pid ${pid}`);
                        this.children[pid].killed = true;
                    }
                    else {
                        this.warning(`Cannot kill child with pid ${pid}!`);
                    }
                }
            }
        }

        // Spawn sox process
        // msg (if set) is forwarded to the next node, when playing sound completes.
        spawnSox(msg = null) {
            let args = [msg?.file ?? this.defaultFile, "remix"].concat(msg?.remix ?? this.defaultRemix);
            this.debug(`Spawning sox with args: ${args}`);
            // this.debug(`Spawning sox with file: ${file}`);
            // this.debug(`Spawning sox with remix: ${remix}`);
            let child = spawn('play', args, { stdio: 'ignore' });
            let pid = child.pid;
            child.on('spawn', () => {
                this.debug(`Spawned sox process with pid ${pid}`);
                this.status({fill: "green", shape: "dot", text: "playing"});
                this.children[pid] = {
                    process: child,
                    killed: false
                }
            })
            .on('exit', (code) => {
                if (code) {
                    this.error(`Sox failed with code ${code}`);
                }
                else {
                    this.debug("Sox exited normally");
                    if (msg && this.children[pid] && !this.children[pid].killed) {
                        this.send(msg);
                    }
                }
                delete this.children[pid];
                if (!Object.keys(this.children).length) {
                    this.status({});
                }
            })
            .on('error', (code) => {
                this.error(`Sox spawn error: ${code}`);
                delete this.children[pid];
                if (!Object.keys(this.children).length) {
                    this.status({});
                }
            });
        }
    }

    RED.nodes.registerType("play-multichannel-audio", PlayMultichannelAudioNode);

    RED.httpAdmin.post("/play-multichannel-audio/:id", RED.auth.needsPermission("play-multichannel-audio.write"), function(req, res) {
        var node = RED.nodes.getNode(req.params.id);
        if (node != null) {
            try {
                if (req.body) {
                    node.receive(req.body);
                } else {
                    node.receive();
                }
                res.sendStatus(200);
            } catch(err) {
                res.sendStatus(500);
                node.error(RED._("inject.failed",{error: err.toString()}));
            }
        }
        else {
            res.sendStatus(404);
        }
    });
}
