module.exports = function(RED) {
    let http = require('http');
    let querystring = require('querystring');

    class OladNode {
        constructor(n) {
            RED.nodes.createNode(this, n);
            this._name = n.name;
            this._host = n.host;
            this._port = n.port;
        }

        get name() {
            return this._name;
        }

        get host() {
            return this._host;
        }

        get port() {
            return this._port;
        }

        /**
         * Send DMX values
         * @param {Number} universeId
         * @param {Array} dmxBuffer
         */
        sendDmx(universeId, dmxBuffer) {
            let post_data = querystring.stringify({
                u: universeId,
                d: Array.prototype.join.call(dmxBuffer)
            });

            let post_options = {
                host: this._host,
                port: this._port,
                path: '/set_dmx',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': Buffer.byteLength(post_data)
                }
            };

            let post_req = http.request(post_options);

            post_req.on('error', (e) => {
                this.error("Error performing request to OLA: " + e.message);
            });

            post_req.write(post_data);
            post_req.end();
        }
    }
    RED.nodes.registerType("olad", OladNode);
}
