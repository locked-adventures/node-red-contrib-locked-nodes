module.exports = function(RED) {
    class DigioOutNode {
        constructor(n) {
            RED.nodes.createNode(this, n);
            this._name = n.name;

            /** @type{DigioConnectionNode} */ this._digio = RED.nodes.getNode(n.digio);
            this._values = JSON.parse(n.values);

            this.on('input', this._onNodeInput.bind(this));
            this._digio.on('digio-status', s => this.status(s))
            this.status(this._digio.digioStatus);
        }

        _onNodeInput(msg) {
            if (msg.payload == 'start' || msg.payload == 'inject') {
                if ('out' in msg) {
                    this._digio.setOutput(msg["out"]);
                }
                else {
                    this._digio.setOutput(this._values);
                }
            }
            if (msg.payload != 'inject') {
                this.send(msg);
            }
        }
    }

    RED.nodes.registerType("digio out", DigioOutNode);

    RED.httpAdmin.post("/digio_out/:id", RED.auth.needsPermission("digio_out.write"), function(req, res) {
        var node = RED.nodes.getNode(req.params.id);
        if (node != null) {
            try {
                if (req.body) {
                    node.receive(req.body);
                } else {
                    node.receive();
                }
                res.sendStatus(200);
            } catch(err) {
                res.sendStatus(500);
                node.error(RED._("inject.failed",{error: err.toString()}));
            }
        }
        else {
            res.sendStatus(404);
        }
    });
};
