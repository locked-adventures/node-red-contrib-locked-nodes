module.exports = function(RED) {
    function myJsonParse(str) {
        if (str == "") {
            return null;
        }
        else {
            return JSON.parse(str);
        }
    }

    class FunctionGeneratorNode {
        constructor(config) {
            RED.nodes.createNode(this, config);

            this.waveProps = {
                waveform: config.waveform,
                frequency: myJsonParse(config.frequency),
                samplingRate: myJsonParse(config.samplingRate),
                min: myJsonParse(config.min),
                max: myJsonParse(config.max),
                initialPhase: myJsonParse(config.initialPhase),
                sendInitialSample: config.sendInitialSample,
                dutyCycle: myJsonParse(config.dutyCycle)
            };

            // Merged config from node properties and message.
            // Configurations explicitly set in the message have priority.
            this.cfg = this.waveProps;

            this.hInterval = null;
            this.period = 1 / this.cfg.frequency;
            this.samplingPeriod = 1 / this.cfg.samplingRate;
            this.running = false;
            this.i = 0; // counter
            this.topic = null;

            this.on('input', function(msg) {
                let payload = msg.payload;
                this.topic = msg.topic;
                if (payload == 'start') {
                    this.mergeWaveConfig(msg);
                    this.start();
                }
                else if (payload == 'stop') {
                    this.stop();
                }
                else if (payload == 'toggle') {
                    if (this.running) {
                        this.stop();
                    }
                    else {
                        this.mergeWaveConfig(msg);
                        this.start();
                    }
                }
            });

            this.status({fill: "red", shape: "ring", text: "idle"});
        }

        mergeWaveConfig(msg) {
            this.cfg = {...this.waveProps, ...msg.wave};
            this.period = 1 / this.cfg.frequency;
            this.samplingPeriod = 1 / this.cfg.samplingRate;
        }

        stop() {
            clearInterval(this.hInterval);
            this.i = 0;
            this.running = false;
            this.status({fill: "red", shape: "ring", text: "idle"});
        }

        _get_time() {
            return this.i * this.samplingPeriod;
        }

        start() {
            this.stop();
            this.running = true;
            this.status({fill: "green", shape: "dot", text: "running"});
            if (this.cfg.sendInitialSample) {
                this.send({topic: this.topic, payload: this.wave(0), i: 0, t: 0});
            }
            this.hInterval = setInterval(this.update.bind(this), 1000 * this.samplingPeriod);
        }

        wave(t) {
            let a = ((t % this.period) / this.period + this.cfg.initialPhase) % 1.0;
            let val = null;
            let min = this.cfg.min;
            let max = this.cfg.max;
            let dutyCycle = this.cfg.dutyCycle;
            switch(this.cfg.waveform) {
                case "sine":
                    let alpha = 2 * a * Math.PI;
                    let b = (min + max) / 2;
                    let m = (max - min) / 2;
                    val = m * Math.sin(alpha) + b;
                    // Clipping just in case floats mess up
                    val = Math.min(val, max);
                    val = Math.max(val, min);
                    break;
                case "square":
                    val = a < dutyCycle ? max : min;
                    break;
                case "triangle":
                    if (a < dutyCycle) {
                        val = min + (max - min) * a / dutyCycle;
                    }
                    else {
                        val = min + (max - min) * (1 - a) / (1 - dutyCycle);
                    }
                    break;
                default:
                    this.error("No such wave type!");
                    break;
            }
            return val;
        }

        update() {
            this.i++;
            let t = this._get_time();
            this.send({topic: this.topic, payload: this.wave(t), i: this.i, t: t});
        }
    }

    RED.nodes.registerType("function generator", FunctionGeneratorNode);

    RED.httpAdmin.post("/function_generator/:id", RED.auth.needsPermission("function_generator.write"), function(req, res) {
        var node = RED.nodes.getNode(req.params.id);
        if (node != null) {
            try {
                if (req.body) {
                    node.receive(req.body);
                } else {
                    node.receive();
                }
                res.sendStatus(200);
            } catch(err) {
                res.sendStatus(500);
                node.error(RED._("inject.failed",{error: err.toString()}));
            }
        }
        else {
            res.sendStatus(404);
        }
    });
};
