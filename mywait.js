module.exports = function(RED) {
    class MyWaitNode {
        constructor(config) {
            RED.nodes.createNode(this, config);

            this.duration = JSON.parse(config.duration);
            this.timeoutId = null;

            this.status({});

            this.on('input', function(msg) {
                let payload = msg.payload;
                if (payload == 'start') {
                    clearTimeout(this.timeoutId);
                    this.status({fill: "green", shape: "dot", text: "waiting..."});
                    this.timeoutId = setTimeout(() => {
                        this.status({});
                        this.send(msg);
                    }, this.duration);
                }
                else if (payload == "stop") {
                    clearTimeout(this.timeoutId);
                    this.status({});
                    this.send(msg);
                }
            });

            this.on('close', function() {
                clearTimeout(this.timeoutId);
                this.status({});
            });
        }
    }

    RED.nodes.registerType("mywait", MyWaitNode);
};
