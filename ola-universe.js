module.exports = function(RED) {
    /**
     * Modulo operation
     * @param {Number} n
     * @param {Number} d
     * @returns n mod d
     */
    function mod(n, d) {
        return ((n % d) + d) % d;
    }

    class OlaUniverseNode {
        constructor(n) {
            RED.nodes.createNode(this, n);
            this._daemon = RED.nodes.getNode(n.daemon);
            this._uid = Number(n.uid);
            this._size = Number(n.size);
            this._buffer = Array(this._size).fill(0);
        }

        /**
         * Ola daemon handle
         */
        get daemon() {
            return this._daemon;
        }

        /**
         * Numeric universe ID
         */
        get uid() {
            return this._uid;
        }

        /**
         * Number of channels
         */
        get size() {
            return this._size;
        }

        /**
         * Send DMX values
         * @param {Object} chanValMap - keys are the channel numbers, values are the numeric dmx values
         */
        sendDmx(chanValMap) {
            if (typeof chanValMap == "object") {
                for (let [chan, val] of Object.entries(chanValMap)) {
                    this._buffer[chan] = mod(val, 256);
                }
                this._daemon.sendDmx(this._uid, this._buffer);
            }
        }
    }
    RED.nodes.registerType("ola-universe", OlaUniverseNode);
}
